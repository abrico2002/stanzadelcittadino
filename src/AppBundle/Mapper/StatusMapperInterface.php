<?php

namespace AppBundle\Mapper;


interface StatusMapperInterface
{
    public function map($status);
}
