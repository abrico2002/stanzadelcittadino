# To get started with security, check out the documentation:
# http://symfony.com/doc/current/book/security.html
security:
  encoders:
    FOS\UserBundle\Model\UserInterface: bcrypt
    Symfony\Component\Security\Core\User\User: plaintext
  providers:
    chain_provider:
      chain:
        providers: [ in_memory, fos_userbundle, cps ]
    in_memory:
      memory:
        users:
          ez:
            password: '%ez_password%'
            roles: 'ROLE_EZ'
          giscom:
            password: '%giscom_password%'
            roles: 'ROLE_GISCOM'
    fos_userbundle:
      id: fos_user.user_provider.username
    cps:
      id: ocsdc.cps.userprovider

  firewalls:
    dev:
      pattern: ^/(_(profiler|wdt)|css|images|js)/
      security: false

    #cps:
    #    pattern: ^/%prefix%/(pratiche|terms_accept|user|documenti)/
    #    guard:
    #        provider: cps
    #        authenticators:
    #            - ocsdc.cps.token_authenticator

    api_login:
      pattern: ^/%prefix%/api/auth
      stateless: true
      anonymous: true
      provider: chain_provider
      json_login:
        check_path: /%prefix%/api/auth
        require_previous_session: false
        success_handler: lexik_jwt_authentication.handler.authentication_success
        failure_handler: lexik_jwt_authentication.handler.authentication_failure

    api:
      pattern: /%prefix%/api/(services|services-groups|calendars|status)
      methods: [ POST, PUT, PATCH, DELETE ]
      stateless: true
      provider: fos_userbundle
      guard:
        authenticators:
          - lexik_jwt_authentication.jwt_token_authenticator

    api_secure:
      pattern: /%prefix%/api/(folders|documents|users|meetings|applications)
      stateless: true
      provider: fos_userbundle
      guard:
        authenticators:
          - lexik_jwt_authentication.jwt_token_authenticator

    operatori:
      logout_on_user_change: true
      anonymous: ~
      pattern: ^/%prefix%/(operatori|admin)
      form_login:
        check_path: fos_user_security_check
        login_path: fos_user_security_login
        provider: fos_userbundle
        csrf_token_generator: security.csrf.token_manager
        success_handler: ocsdc.redirect.after.login
      logout:
        path: logout
        target: /%prefix%/

    giscom_api:
      logout_on_user_change: true
      pattern: ^/%prefix%/api/v\d+\.\d+/giscom/pratica/.*/.*
      http_basic:
        provider: in_memory

    ez_api:
      logout_on_user_change: true
      pattern: ^/%prefix%/api/v\d+\.\d+/schedaInformativa/.*
      http_basic:
        provider: in_memory

    print:
      logout_on_user_change: true
      pattern: ^/%prefix%/print/pratica/.*
      http_basic:
        provider: in_memory

    open_login:
      logout_on_user_change: true
      anonymous: true
      provider: cps
      logout:
        path: user_logout #name of the route
        target: /%prefix%/
        delete_cookies: [ '_forward_auth_csrf', '_forward_auth_spid', 'PHPSESSID', 'token', 'refresh_token' ]
        success_handler: ocsdc.logout_success_handler
      guard:
        authenticators:
          - ocsdc.pat.authenticator
          - ocsdc.openlogin.authenticator
        entry_point: ocsdc.pat.authenticator

    main:
      logout_on_user_change: true
      anonymous: ~

  role_hierarchy:
    ROLE_ADMIN: ROLE_USER
    ROLE_OPERATORE_ADMIN: ROLE_OPERATORE
    ROLE_OPERATORE: ROLE_USER

  access_control:
    - { path: ^/%prefix%/servizi/, role: IS_AUTHENTICATED_ANONYMOUSLY }
    - { path: ^/%prefix%/operatori/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
    - { path: ^/%prefix%/api/doc, role: IS_AUTHENTICATED_ANONYMOUSLY }
    - { path: ^/%prefix%/operatori/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }

    - { path: ^/%prefix%/(pratiche|terms_accept|user|documenti|subscriptions)/, role: ROLE_USER }
    - { path: ^/%prefix%/api/v\d+\.\d+/user/.*, role: ROLE_USER }

    - { path: ^/%prefix%/operatori/profile, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/profile/edit, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/profile/change-password, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/subscriptions, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/subscription-service, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/subscriber, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori/calendars, role: [ ROLE_OPERATORE, ROLE_ADMIN ] }
    - { path: ^/%prefix%/operatori, role: ROLE_OPERATORE }
    - { path: ^/%prefix%/api/v\d+\.\d+/pratica/.*/.*, role: ROLE_GISCOM }
    - { path: ^/%prefix%/api/v\d+\.\d+/schedaInformativa/.*, role: ROLE_EZ }
    - { path: ^/%prefix%/admin, role: ROLE_ADMIN }
